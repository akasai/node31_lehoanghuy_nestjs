import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy,"jwt_token") {
    constructor(config: ConfigService) {
        super({
            jwtFromRequest:
                ExtractJwt.fromAuthHeaderAsBearerToken(), 
        ignoreExpiration: false,
        secretOrKey:config.get("SECRET_KEY"), //NODE31
        });
    }

    // giải mã và trả về dữ liệu khi token hợp lệ cho controller
    async validate(tokenDecode: any) {
        return tokenDecode;
    }
}