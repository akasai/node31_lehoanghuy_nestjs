import { ApiProperty } from "@nestjs/swagger"

class danhSachVe {
    @ApiProperty({type: Number})
    ma_ghe :number

    @ApiProperty({type: Number})
    gia_ve :number
}

class bookingTicketType  {

    @ApiProperty({type: Number})
    ma_lich_chieu:number 

    @ApiProperty({ type: [danhSachVe] }) 
    danh_sach_ve: danhSachVe[]

}

class createShowTimeType  {

    @ApiProperty({type: Number})
    ma_phim:number 

    @ApiProperty({ type: String}) 
    ngay_gio_chieu: string

    @ApiProperty({ type: Number}) 
    ma_rap: number

    @ApiProperty({ type: Number}) 
    gia_ve: number

}


export {
    bookingTicketType,
    createShowTimeType
}