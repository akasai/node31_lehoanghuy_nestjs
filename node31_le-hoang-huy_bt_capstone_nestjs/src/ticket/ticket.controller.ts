import { Body, Controller, Get, Post, Query, Req, UseGuards } from '@nestjs/common';
import { TicketService } from './ticket.service';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { bookingTicketType, createShowTimeType } from './entities/ticket.entities';


@ApiTags("QuanLyDatVe")
@Controller('ticket')
export class TicketController {
  constructor(private readonly ticketService: TicketService) {}

  //dat ve
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Post("book-ticket")
  bookTicket(
    @Req() req,
    @Body() bookTicket:bookingTicketType 
  ){
    let account = req.user.data.tai_khoan;
    return this.ticketService.bookTicket(account,bookTicket);
  }

  //lấy danh sách ve theo mã lịch chiếu
  @ApiQuery({name:"ma_lich_chieu",required:false})
  @Get("get-list-ticket")
  getListTicket(
    @Query("ma_lich_chieu") ma_lich_chieu :string,
  ){
    return this.ticketService.getListTicket(Number(ma_lich_chieu));
  }

  //tạo lịch chiếu theo mã phim
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Post("create-showtime")
  createMovieShowTime(
    @Req() req,
    @Body() showTime:createShowTimeType 
  ){
    let role = req.user.data.vai_tro
    console.log(role)
    if(role !== "Admin"){
      return "UserRole doesnt have permission to create Show Time"
    }else{
      return this.ticketService.createMovieShowTime(showTime);
    }
  }


}
