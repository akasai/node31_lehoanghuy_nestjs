import { Module } from '@nestjs/common';
import { TicketService } from './ticket.service';
import { TicketController } from './ticket.controller';
import { ResponeService } from 'src/message/res.message';

@Module({
  controllers: [TicketController],
  providers: [TicketService,ResponeService]
})
export class TicketModule {}
