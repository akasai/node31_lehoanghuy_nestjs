import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { ResponeService } from 'src/message/res.message';
import * as moment from 'moment';
import { bookingTicketType, createShowTimeType } from './entities/ticket.entities';

@Injectable()
export class TicketService {
    prisma = new PrismaClient();
    constructor(private resService : ResponeService){}

    async getListTicket(ma_lich_chieu:number) {
        let data = await this.prisma.lich_chieu.findFirst(
           {
                where :{
                    ma_lich_chieu
                },
                include : {
                    phim :true,
                    rap_phim : {
                        include :{
                            ghe :true,
                            cum_rap :true
                        }
                    },
                }
           }
        );

        if(data){
            let thongTinPhim = {
                maLichChieu: ma_lich_chieu,
                tenCumRap  : data.rap_phim.cum_rap.ten_cum_rap ,
                tenRap     : data.rap_phim.ten_rap,
                diaChi     : data.rap_phim.cum_rap.dia_chi,
                tenPhim    : data.phim.ten_phim,
                hinhAnh    : data.phim.hinh_anh,
                ngayChieu  : moment(data.ngay_gio_chieu).format('DD/MM/YYYY'),
                gioChieu   : moment(data.ngay_gio_chieu).format('HH:mm:ss') 
            }
    
            let cloneGhe = Object.assign({}, data.rap_phim.ghe);
            let danhSachGhe =[]
            for(let i = 0;i<Object.keys(cloneGhe).length;i++){
                let key = Object.keys(cloneGhe)[i]
                if(cloneGhe[key].loai_ghe==="Vip")
                {
                    cloneGhe[key].giaVe= (data.gia_ve*120)/100
                }else{
                    cloneGhe[key].giaVe= data.gia_ve
                }
                danhSachGhe.push(cloneGhe[key]);
            }
            
            let dataRes = {thongTinPhim,danhSachGhe}
            return this.resService.successCode(dataRes,"Get Success");
        }else{
            return this.resService.failCode([],"dataShowTime not found");
        }
       
    }

    async bookTicket(account:string,bookTicket:bookingTicketType){
        let dataUser = await this.prisma.nguoi_dung.findFirst({
            where : {
                tai_khoan : account
            }
        })

       
        let dataRes = []
        for(let i = 0; i<bookTicket.danh_sach_ve.length; i++){
            let dataBooking = {
                ma_tai_khoan : dataUser.ma_tai_khoan,
                ma_lich_chieu : bookTicket.ma_lich_chieu,
                ma_ghe: 0
            }

            dataBooking.ma_ghe = bookTicket.danh_sach_ve[i].ma_ghe
            let checkSeat = await this.prisma.ghe.findFirst({
                where :{
                    ma_ghe:dataBooking.ma_ghe
                }
            })
            if(checkSeat.is_available){
                await this.prisma.dat_ve.create({
                    data: dataBooking
                })

                dataRes.push(dataBooking);
                let newData = await this.prisma.ghe.findFirst({
                where :{
                    ma_ghe:dataBooking.ma_ghe
                }
                })

                newData.is_available = false
                await this.prisma.ghe.update({
                   data: newData,
                   where :{
                      ma_ghe:dataBooking.ma_ghe
                   }
                })
            }else{
                return this.resService.failCode(dataBooking.ma_ghe,"This seat is not available"); 
            } 
        }

        return this.resService.successCode(dataRes,"Successfully book ticket"); 
      
    }

    async createMovieShowTime(showTime : createShowTimeType){
        let checkCinema = await this.prisma.rap_phim.findFirst({
            where: {
                ma_rap: showTime.ma_rap
            }
        })

        if(checkCinema){
            let checkMovie = await this.prisma.phim.findFirst({
                where: {
                    ma_phim: showTime.ma_phim
                }
            })
            
            if(checkMovie){
                let time = new Date(showTime.ngay_gio_chieu);
                let newData = {...showTime,ngay_gio_chieu:time}
                await this.prisma.lich_chieu.create({
                    data:newData
                })
                return this.resService.successCode(newData,"Successfully create Show Time"); 
            }else{
                return this.resService.failCode([],"Movie ID not found");
            }
        }else{
            return this.resService.failCode([],"Cinema ID not found");
        }

        
    }
}
