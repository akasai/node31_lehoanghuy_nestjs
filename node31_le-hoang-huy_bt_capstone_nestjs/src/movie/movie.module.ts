import { Module } from '@nestjs/common';
import { MovieService } from './movie.service';
import { MovieController } from './movie.controller';
import { ResponeService } from 'src/message/res.message';

@Module({
  controllers: [MovieController],
  providers: [MovieService,ResponeService]
})
export class MovieModule {}
