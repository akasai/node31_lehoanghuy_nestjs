import { Injectable } from '@nestjs/common';
import { ResponeService } from 'src/message/res.message';
import { PrismaClient} from '@prisma/client';
import * as moment from 'moment';
import { movieDto } from './entities/movie.entities';

@Injectable()
export class MovieService {

    prisma = new PrismaClient();

    constructor(
        private resService : ResponeService
        ){}
    
    async getAllBanner() {
        let data = await this.prisma.banner.findMany();
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
           
    }

    async getMovie(ten_phim:string) {
        let data;
        if(ten_phim ===undefined){
            data = await this.prisma.phim.findMany();
        }else{
            data = await this.prisma.phim.findMany({
                where :{
                    ten_phim : {
                        contains :ten_phim
                    }
                }
            });
        }
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
           
    }

    async getMovieWithOffSet(ten_phim:string, page: number, pageSize: number) {
        console.log(ten_phim)
        let index = (page-1)*pageSize
        let listAllItems = await this.prisma.phim.findMany();
        let data = await this.prisma.phim.findMany({
            where:{
                ten_phim:{
                    contains:ten_phim
                }
            },
            skip: index,
            take: pageSize
        });
        if(data.length>0){
            let resData = {
                currentPage: page,
                totalPages : listAllItems.length/pageSize<pageSize?1:Math.ceil(listAllItems.length/pageSize),
                totalCount:  listAllItems.length,
                listMovie :  data
            }
            return this.resService.successCode(resData,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }      
    }

    async getMovieWithOffSetByDate(startDate:string,endDate:string, page: number, pageSize: number) {
        let index = (page-1)*pageSize
        let tu_ngay = new Date(startDate);
        let den_ngay = new Date(endDate);

        if(!moment(tu_ngay).isValid() || !moment(den_ngay).isValid()){
            return this.resService.failCode([],"Date Input invalid in StartDate or EndDate (format : YYYY/MM/DD or YYYT-MM-DD)");
        }else{
            let data = await this.prisma.phim.findMany({
                where:{
                    ngay_khoi_chieu:{
                        gte:tu_ngay,
                        lte:den_ngay
                    }
                },
                skip: index,
                take: pageSize
            });
    
            console.log(data);
            if(data.length>0){
                let resData = {
                    currentPage: page,
                    totalPages : data.length/pageSize<pageSize?1:Math.ceil(data.length/pageSize),
                    totalCount:  data.length,
                    listMovie :  data
                }
                return this.resService.successCode(resData,"Get Success");
            }else{
                return this.resService.failCode([],"Data not found");
            } 
        }
             
    }

    async addMovie(movie_info :movieDto , fileUpload : Express.Multer.File){
        let showTimeDate = new Date(movie_info.ngay_khoi_chieu);
        if(!moment(showTimeDate).isValid()){
            return this.resService.failCode([],"Date Input invalid (format : YYYY/MM/DD or YYYT-MM-DD)");
        }else{
            let dataNewMovie = {       
                ten_phim        : movie_info.ten_phim,      
                trailer         : movie_info.trailer,           
                hinh_anh        : fileUpload.filename,
                mo_ta           : movie_info.mo_ta,         
                ngay_khoi_chieu : showTimeDate, 
                danh_gia        : Number(movie_info.dang_chieu),
                hot             : Number(movie_info.hot)===0?false:true,
                dang_chieu      : Number(movie_info.dang_chieu)===0?false:true,
                sap_chieu       : Number(movie_info.sap_chieu)===0?false:true,
            }
            await this.prisma.phim.create({
                data: dataNewMovie
            })
            return this.resService.successCode(dataNewMovie,"Get Success");
        } 
    }

    
    async updateMovie(ma_phim:number,movie_info :movieDto , fileUpload : Express.Multer.File){
        let checkMovie = await this.prisma.phim.findFirst({
            where:{
                ma_phim
            }
        })

        if(checkMovie){
            let showTimeDate = new Date(movie_info.ngay_khoi_chieu);
            if(!moment(showTimeDate).isValid()){
                return this.resService.failCode([],"Date Input invalid (format : YYYY/MM/DD or YYYT-MM-DD)");
            }else{
                let dataNewMovie = {       
                    ten_phim        : movie_info.ten_phim,      
                    trailer         : movie_info.trailer,           
                    hinh_anh        : fileUpload.filename,
                    mo_ta           : movie_info.mo_ta,         
                    ngay_khoi_chieu : showTimeDate, 
                    danh_gia        : Number(movie_info.danh_gia),
                    hot             : Number(movie_info.hot)===0?false:true,
                    dang_chieu      : Number(movie_info.dang_chieu)===0?false:true,
                    sap_chieu       : Number(movie_info.sap_chieu)===0?false:true,
                }
               
                await this.prisma.phim.update({
                    data: dataNewMovie,
                    where :{
                        ma_phim
                    }
                })
                return this.resService.successCode(dataNewMovie,"Get Success");
            } 
        }else{
            return this.resService.failCode([],"MovieID not found");
        }
          
    }

    async removeMovie(ma_phim: number) {
        let checkMovie =  await this.prisma.phim.findFirst({
            where :{
                ma_phim
            }
        })
        if(checkMovie){
            await this.prisma.phim.delete({
                where :{
                  ma_phim
                }
              })
              return this.resService.successCode(ma_phim,`MovieID #${ma_phim} has been deleted`);
        }else{
            return this.resService.failCode([],"MovieID not found");
        }
       
    }

    async getMovieInfo(ma_phim: number) {
        let data = await this.prisma.phim.findFirst({
          where :{
            ma_phim
          }
        })
        if(data){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
        
    }
}
