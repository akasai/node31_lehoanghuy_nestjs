import { Body, Controller, Delete, Get, Post, Put, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { MovieService } from './movie.service';
import { ApiBearerAuth,ApiBody,ApiConsumes,ApiQuery, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { movieDto} from './entities/movie.entities';


@ApiTags("QuanLyPhim")
@Controller('movie')
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

   //lấy danh sách BANNER phim
  @Get("get-all-banner")
  getAllBanner(){
    return this.movieService.getAllBanner();
  }

  //lấy danh sách phim theo tên phim, tên Rạp phim
  @ApiQuery({name: "ten_phim", required:false})
  @Get("get-movie")
  getMovie(
    @Query('ten_phim') ten_phim: string,
  ){
    return this.movieService.getMovie(ten_phim);
  }

  //lay danh sách phim phân trang
  @ApiQuery({name: "ten_phim", required:false})
  @Get("get-movie-offset")
  getMovieWithOffSet(
    @Query('ten_phim') ten_phim: string,
    @Query('page') page: string,
    @Query('pageSize') pageSize: string
  ){
    if(Number.isNaN(+page) || Number.isNaN(+pageSize)){
      return "page/pageSize must be a number"
    }else{
      if(Number(page)===0){
        return "page must be greater than 0"
      }else{
        return this.movieService.getMovieWithOffSet(ten_phim,Number(page),Number(pageSize));
      }  
    }
  }

  //lay danh sách phim phân trang theo ngày
  @ApiQuery({name: "tu_ngay", required:false})
  @ApiQuery({name: "den_ngay", required:false})
  @Get("get-movie-offset-by-date")
  getMovieWithOffSetByDate(
    @Query('tu_ngay') startDate: string,
    @Query('den_ngay') endDate: string,
    @Query('page') page: string,
    @Query('pageSize') pageSize: string
  ){
    if(Number.isNaN(+page) || Number.isNaN(+pageSize)){
      return "page/pageSize must be a number"
    }else{
      if(Number(page)===0){
        return "page must be greater than 0"
      }else{
        return this.movieService.getMovieWithOffSetByDate(startDate,endDate,Number(page),Number(pageSize));
      }  
    }
  }

  //thêm phim upload hình ảnh
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @UseInterceptors(FileInterceptor("file",{
    storage:diskStorage({
        destination:process.cwd()+"/public/img",
        filename:(req,file, callback)=> callback(null, new Date().getTime()+"_"+file.originalname)
    })
  }))
  @ApiConsumes("multipart/form-data")
  @ApiBody({
      type: movieDto
  })
  @Post("add-movie")
  addMovie(
    @Body() movie_info :movieDto,
    @UploadedFile() fileUpload:Express.Multer.File,
    @Req() req
  ){
    
    let role = req.user.data.vai_tro
    console.log(role)
    if(role !== "Admin"){
      return "UserRole doesnt have permission to add movie"
    }else{
      return this.movieService.addMovie(movie_info,fileUpload)
    }
  }

  //update thông tin phim ,upload hình ảnh
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @UseInterceptors(FileInterceptor("file",{
    storage:diskStorage({
        destination:process.cwd()+"/public/img",
        filename:(req,file, callback)=> callback(null, new Date().getTime()+"_"+file.originalname)
    })
  }))
  @ApiConsumes("multipart/form-data")
  @ApiBody({
      type: movieDto
  })
  @Put("update-movie")
  updateMovie(
    @Req() req,
    @Query("ma_phim") ma_phim :string,
    @Body() movie_info :movieDto,
    @UploadedFile() fileUpload:Express.Multer.File
  ){
    let role = req.user.data.vai_tro
    console.log(role)
    if(role !== "Admin"){
      return "UserRole doesnt have permission to update movie"
    }else{
      return this.movieService.updateMovie(Number(ma_phim),movie_info,fileUpload)
    }
  }

  //xóa người dung
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Delete("remove-movie")
  removeMovie(
    @Req() req,
    @Query("ma_phim") ma_phim :string
  ){
    let role = req.user.data.vai_tro
    console.log(role)
    if(role !== "Admin"){
      return "UserRole doesnt have permission to delete movie"
    }else{
      return this.movieService.removeMovie(Number(ma_phim));
    }
   
  }

  //lấy thông phim theo mã phim, tên Rạp phim
  @ApiQuery({name: "ma_phim"})
  @Get("get-movie-info")
  getMovieInfo(
    @Query('ma_phim') ma_phim: string,
  ){
    return this.movieService.getMovieInfo(Number(ma_phim));
  }
    
}
