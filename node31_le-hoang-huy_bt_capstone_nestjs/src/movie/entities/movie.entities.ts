import { ApiProperty } from "@nestjs/swagger"

class movieDto  {
    @ApiProperty({type: String})
    ten_phim:string

    @ApiProperty({type: String, required :false})
    trailer:string

    @ApiProperty({type: String, required :false})
    mo_ta:string

    @ApiProperty({type: String})
    ngay_khoi_chieu:string

    @ApiProperty({type: Number, required :false})
    danh_gia:number

    @ApiProperty({type: Number, required :false})
    hot:number

    @ApiProperty({type: Number, required :false})
    dang_chieu:number

    @ApiProperty({type: Number,required :false})
    sap_chieu:number

    @ApiProperty({type:String,format:"binary"})
    file:Express.Multer.File 
}

export {
    movieDto,
}
