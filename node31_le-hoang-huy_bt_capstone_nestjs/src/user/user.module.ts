import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from 'src/strategy/jwt.strategy';
import { ResponeService } from 'src/message/res.message';


@Module({
  imports:[JwtModule.register({})],
  controllers: [UserController],
  providers: [UserService,JwtStrategy,ResponeService]
})
export class UserModule {}
