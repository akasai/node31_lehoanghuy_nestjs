import { Body, Controller, Get, HttpException, HttpStatus, Post, UseGuards, Req, Put, Delete, Query } from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBearerAuth,ApiQuery, ApiTags } from '@nestjs/swagger';
import { addUserType, updateUserInfoByUserType, updateUserInfoType, userLoginType, userSignUpType } from './entities/user.entities';
import { AuthGuard } from '@nestjs/passport';


@ApiTags("QuanLyNguoiDung")
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService
    ) {}

  //lấy danh sách mã loại người dùng
  @Get("get-all-type-users")
  getAllTypeUsers(){
    return this.userService.getAllTypeUsers();
  }

  //đăng nhập
  @Post("login")
  login(@Body() userLogin:userLoginType){
      return this.userService.login(userLogin);
  }

  //đăng ký
  @Post("sign-up")
  signUp(@Body() userSignUp:userSignUpType ){
    return this.userService.signUp(userSignUp);
  }

  //danh sách tất cả người dùng theo ma nhom
  @ApiQuery({name:"ma_nhom",required:false})
  @Get("get-all-users")
  getAllUsers(
    @Query("ma_nhom") ma_nhom :string,
  ){
    return this.userService.getAllUsers(ma_nhom);
  }

  //danh sách tất cả người dùng theo ma nhom người dùng phân trang
  @ApiQuery({name:"ma_nhom",required:false})
  @Get("get-all-users-offset")
  getAllUsersByGroupWithOffset(
    @Query("ma_nhom") ma_nhom :string,
    @Query('page') page: string,
    @Query('pageSize') pageSize: string
  ){
    try{
      if(Number.isNaN(+page) || Number.isNaN(+pageSize)){
        return "type_user/page/pageSize must be a number"
      }else{
        if(Number(page)===0){
          return "page must be greater than 0"
        }else{
          return this.userService.getAllUsersByGroupWithOffset(ma_nhom,Number(page),Number(pageSize));
        }
      }
    }catch{
      throw new HttpException("Bad request",HttpStatus.BAD_REQUEST) 
    }
    
  }

  //tìm người dùng theo tên tài khoản
  @Get("search-user")
  getUserByAccount(
    @Query("account") tai_khoan:string
  ){
    console.log('getUserByAccount ',tai_khoan);
    return this.userService.getUserByAccount(tai_khoan);
  }

  //tìm người dùng theo ho ten phân trang
  @Get("search-user-offset")
  getUserByAccountWithOffSet(
    @Query("account") tai_khoan :string,
    @Query('page') page: string,
    @Query('pageSize') pageSize: string
  ){
    if(Number.isNaN(+page) || Number.isNaN(+pageSize)){
      return "page/pageSize must be a number"
    }else{
      if(Number(page)===0){
        return "page must be greater than 0"
      }else{
        return this.userService.getUserByAccountWithOffSet(tai_khoan,Number(page),Number(pageSize));
      }  
    }
    
  }

  //lấy thông tin người dùng từ token
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Post("get-user-from-token")
  getUserInforFromToken(
    @Req() req
  ){
    let tai_khoan = req.user.data.tai_khoan
    return this.userService.getUserInforFromToken(tai_khoan)
  }

  //lấy thông tin người dùng thông qua tài khoản
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Post("get-user-info")
  getUserInfor(
    @Query('account') tai_khoan: string,
  ){
    return this.userService.getUserInfor(tai_khoan)
  }

  //thêm người dùng
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Post("add-user")
  addUser(
    @Req() req,
    @Body() userInfo:addUserType 
  ){
    let role = req.user.data.vai_tro
    console.log(role)
    if(role !== "Admin"){
      return "UserRole doesnt have permission to add user"
    }else{
      return this.userService.addUser(userInfo);
    }
    
  }

  //update thông tin người dùng bởi admin
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Put("update-user")
  updateUser(
    @Req() req,
    @Body() userInfo:updateUserInfoType 
  ){
    let role = req.user.data.vai_tro
    console.log(role)
    if(role !== "Admin"){
      return "UserRole doesnt have permission to update user"
    }else{
      return this.userService.updateUser(userInfo);
    }
  }

  //update thông tin người dùng bởi chính người dùng
  @ApiBearerAuth()
  @UseGuards(AuthGuard("jwt_token"))
  @Put("update-user-info")
  updateUserInfo(
    @Req() req,
    @Body() userInfo:updateUserInfoByUserType 
  ){
    let account = req.user.data.tai_khoan
    return this.userService.updateUserInfo(account,userInfo);
  }

   //xóa người dung
   @ApiBearerAuth()
   @UseGuards(AuthGuard("jwt_token"))
   @Delete("remove-user")
   removeUser(
    @Query('ma_tai_khoan') id: string
   ){
     return this.userService.removeUser(Number(id));
   }



}
