import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { addUserType, updateUserInfoByUserType, updateUserInfoType, userLoginType, userSignUpType } from './entities/user.entities';
import { PrismaClient} from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { ResponeService } from 'src/message/res.message';

@Injectable()
export class UserService {
    prisma = new PrismaClient();
    constructor(
        private jwtService : JwtService,
        private config : ConfigService,
        private resService : ResponeService
        ){}

    async getAllTypeUsers() {
        let data = await this.prisma.nguoi_dung.findMany(
           {
            distinct: ['vai_tro'],
            select:{
                loai_nguoi_dung:true,
                vai_tro:true,
            }
           }
        );
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
       
    }
    
    async login(userLogin:userLoginType){
       
        let checkUser = await this.prisma.nguoi_dung.findFirst({
            where:{
                tai_khoan: userLogin.tai_khoan
            }
        })
        if(checkUser){
            if(bcrypt.compareSync(userLogin.mat_khau,checkUser.mat_khau)){
                let token = this.jwtService.sign(
                    {data:{tai_khoan:checkUser.tai_khoan,vai_tro:checkUser.vai_tro}},
                    {expiresIn:"30m",secret:this.config.get("SECRET_KEY")
                })
                let resData = {...checkUser,mat_khau:"",access_token:token}
                return this.resService.successCode(resData,"Login Success");
            }else{
                return this.resService.failCode([],"Wrong Password,pls check it");
            }  
        }else{
            return this.resService.failCode([],"Account is not exist");
        }
    }
  
    async signUp(userSignUp:userSignUpType){
        //check account trung
        let checkAccount = await this.prisma.nguoi_dung.findMany({
            where:{
                tai_khoan: userSignUp.tai_khoan
            }
        })
        if(checkAccount.length>0){
            return this.resService.failCode([],"Account already exist");
        }else{
            let newUser ={
                tai_khoan:userSignUp.tai_khoan,
                ho_ten:userSignUp.ho_ten,
                email:userSignUp.email,
                so_dt:userSignUp.so_dt,
                mat_khau:bcrypt.hashSync(userSignUp.mat_khau,10),
                loai_nguoi_dung:1,
                vai_tro:"User",
                ma_nhom:userSignUp.ma_nhom,
            }
            await this.prisma.nguoi_dung.create({data:newUser})
            let resData = {...newUser,mat_khau:""}
            return this.resService.successCode(resData,"Successfully registered");
        }
    }

    async getAllUsers(ma_nhom:string) {
        console.log(ma_nhom)
        let data = await this.prisma.nguoi_dung.findMany({
            where:{
                ma_nhom:{
                    contains:ma_nhom
                }
            },
            select:{
                ma_tai_khoan: true,
                tai_khoan: true,
                ho_ten: true,
                email: true,
                so_dt: true,
                vai_tro: true,
                ma_nhom: true
            }
        });
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
    }

    async getAllUsersByGroupWithOffset(ma_nhom: string, page: number, pageSize: number) {
        let index = (page-1)*pageSize
        let listAllItems = await this.prisma.nguoi_dung.findMany();
        let data = await this.prisma.nguoi_dung.findMany({
            where:{
                ma_nhom:{
                    contains:ma_nhom
                }
            },
            select:{
                ma_tai_khoan: true,
                tai_khoan: true,
                ho_ten: true,
                email: true,
                so_dt: true,
                vai_tro: true,
                ma_nhom: true
            },
            skip: index,
            take: pageSize
        });
        if(data.length>0){
            let resData = {
                currentPage: page,
                totalPages : listAllItems.length/pageSize<pageSize?1:Math.ceil(listAllItems.length/pageSize),
                totalCount:listAllItems.length,
                listUser : data
            }
            return this.resService.successCode(resData,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
        
    }

    async getUserByAccount(tai_khoan:string){
        let data = await this.prisma.nguoi_dung.findMany({
            where:{
                tai_khoan:{
                    contains:tai_khoan
                }
            },
            select:{
                ma_tai_khoan: true,
                tai_khoan: true,
                ho_ten: true,
                email: true,
                so_dt: true,
                vai_tro: true,
                ma_nhom: true
            }
        })
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
    }

    async getUserByAccountWithOffSet(tai_khoan:string, page: number, pageSize: number) {
        let index = (page-1)*pageSize
        let listAllItems = await this.prisma.nguoi_dung.findMany();
        let data = await this.prisma.nguoi_dung.findMany({
            where:{
                tai_khoan:{
                    contains:tai_khoan
                }
            },
            select:{
                ma_tai_khoan: true,
                tai_khoan: true,
                ho_ten: true,
                email: true,
                so_dt: true,
                vai_tro: true,
                ma_nhom: true
            },
            skip: index,
            take: pageSize
        })

        if(data.length>0){
            let resData = {
                currentPage: page,
                totalPages : listAllItems.length/pageSize<pageSize?1:Math.ceil(listAllItems.length/pageSize),
                totalCount: listAllItems.length,
                listUser : data
            }
            return this.resService.successCode(resData,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }
        
    }

    async getUserInforFromToken(tai_khoan){
        console.log(tai_khoan)
        let userInfo = await this.prisma.nguoi_dung.findFirst({
            where:{
                tai_khoan
            },
            select:{
                ma_tai_khoan: true,
                tai_khoan: true,
                ho_ten: true,
                email: true,
                so_dt: true,
                vai_tro: true,
                ma_nhom: true
            }
        })
        console.log(userInfo)
        if(userInfo){
            return this.resService.successCode(userInfo,"Get Success");
        }else{
            return this.resService.failCode([],"User not found");
        }
    }

    async getUserInfor(tai_khoan:string){
        let userInfo = await this.prisma.nguoi_dung.findFirst({
            where:{
                tai_khoan
            },
            select:{
                ma_tai_khoan: true,
                tai_khoan: true,
                ho_ten: true,
                email: true,
                so_dt: true,
                vai_tro: true,
                ma_nhom: true
            }
        })
        if(!userInfo){
            return this.resService.failCode([],"User not found");
        }else{
            return this.resService.successCode(userInfo,"Get Success");
        }
    }

    async addUser(userInfo:addUserType){
        //check account trung
        let checkAccount = await this.prisma.nguoi_dung.findMany({
            where:{
                tai_khoan: userInfo.tai_khoan
            }
        })
        if(checkAccount.length>0){
            return this.resService.failCode([],"Account already exist");
        }else{
            let newUser ={
                tai_khoan:userInfo.tai_khoan,
                ho_ten:userInfo.ho_ten,
                email:userInfo.email,
                so_dt:userInfo.so_dt,
                mat_khau:bcrypt.hashSync(userInfo.mat_khau,10),
                loai_nguoi_dung:Number(userInfo.loai_nguoi_dung),
                vai_tro:Number(userInfo.loai_nguoi_dung)===0?"Admin":"User",
                ma_nhom:userInfo.ma_nhom
            }
            await this.prisma.nguoi_dung.create({data:newUser})
            return this.resService.successCode({...newUser,mat_khau:""},"Successfully add new user");
        }
    }

    async updateUser(userInfo:updateUserInfoType){
        
        let checkUser = await this.prisma.nguoi_dung.findFirst({
            where:{
                ma_tai_khoan:userInfo.ma_tai_khoan
            }
        })

        if(checkUser){
            let newUserInfo ={
                ma_tai_khoan:userInfo.ma_tai_khoan,
                ho_ten:userInfo.ho_ten,
                email:userInfo.email,
                so_dt:userInfo.so_dt,
                mat_khau:bcrypt.hashSync(userInfo.mat_khau,10),
                loai_nguoi_dung:Number(userInfo.loai_nguoi_dung),
                vai_tro:Number(userInfo.loai_nguoi_dung)===0?"Admin":"User",
                ma_nhom:userInfo.ma_nhom
            }
            await this.prisma.nguoi_dung.update({
                data:newUserInfo,
                where:{
                    ma_tai_khoan:userInfo.ma_tai_khoan
                }
            })
            return this.resService.successCode({...newUserInfo,mat_khau:""},"Successfully update user infor");
        }else{
            return this.resService.failCode([],"UserID not found");
        }

        
        
    }

    async updateUserInfo(account,userInfo:updateUserInfoByUserType){
    
        let dataUser = await this.prisma.nguoi_dung.findFirst({
            where:{
                tai_khoan: account
            }
        })

        if(dataUser){
            let newUserInfo ={
                ho_ten:userInfo.ho_ten,
                email:userInfo.email,
                so_dt:userInfo.so_dt,
                mat_khau:bcrypt.hashSync(userInfo.mat_khau,10),
            }
    
            await this.prisma.nguoi_dung.update({
                data:newUserInfo,
                where:{
                    ma_tai_khoan:dataUser.ma_tai_khoan
                }
            })
            return this.resService.successCode({...newUserInfo,mat_khau:""},"Successfully update user infor");
        }else{
            return this.resService.failCode([],"User Account not found");
        }
        
        
    }

    async removeUser(id: number) {
        let checkUser = await this.prisma.nguoi_dung.findFirst({
            where : {
                ma_tai_khoan:id
            }
        })
        console.log(checkUser)
        if(checkUser){
            await this.prisma.nguoi_dung.delete({
                where :{
                  ma_tai_khoan:id
                }
              })
              return `Account #${id} has been deleted`;
        }else{
            return this.resService.failCode([],"User ID not found");
        }
        
      }

}
