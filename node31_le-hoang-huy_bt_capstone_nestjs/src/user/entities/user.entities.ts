import { ApiProperty } from "@nestjs/swagger"
import { IsEmail,IsNotEmpty} from "class-validator"

class userLoginType  {
    @ApiProperty({type: String})
    @IsNotEmpty()
    tai_khoan:string 

    @ApiProperty({type: String})
    @IsNotEmpty()
    mat_khau:string
}

class userSignUpType{
   
    @ApiProperty({type: String})
    @IsNotEmpty()
    tai_khoan: string

    @ApiProperty({type: String})
    @IsNotEmpty()
    mat_khau: string

    @ApiProperty({type: String})
    @IsEmail()
    email: string | null

    @ApiProperty({type: String})
    so_dt: string | null

    @ApiProperty({type: String})
    ho_ten: string | null

    @ApiProperty({type: String})
    ma_nhom: string | null
}

class addUserType{
   
    @ApiProperty({type: String})
    @IsNotEmpty()
    tai_khoan: string

    @ApiProperty({type: String})
    @IsNotEmpty()
    mat_khau: string

    @ApiProperty({type: String})
    @IsEmail()
    email: string | null

    @ApiProperty({type: String})
    so_dt: string | null

    @ApiProperty({type: String})
    ma_nhom: string | null

    @ApiProperty({type: String})
    ho_ten: string | null

    @ApiProperty({description:"0 => Admin / >0 => User",type: Number})
    loai_nguoi_dung: number | null
}

class updateUserInfoType{
    @ApiProperty({type: Number})
    @IsNotEmpty()
    ma_tai_khoan: number

    @ApiProperty({type: String})
    @IsNotEmpty()
    mat_khau: string

    @ApiProperty({type: String})
    @IsEmail()
    email: string | null

    @ApiProperty({type: String})
    so_dt: string | null

    @ApiProperty({type: String})
    ma_nhom: string | null

    @ApiProperty({type: String})
    ho_ten: string | null

    @ApiProperty({description:"0 => Admin / >0 => User",type: Number})
    loai_nguoi_dung: number | null
}

class updateUserInfoByUserType{
    
    @ApiProperty({type: String})
    @IsNotEmpty()
    mat_khau: string

    @ApiProperty({type: String})
    @IsEmail()
    email: string | null

    @ApiProperty({type: String})
    so_dt: string | null

    @ApiProperty({type: String})
    ho_ten: string | null
}


export {
    userLoginType,
    userSignUpType,
    addUserType,
    updateUserInfoType,
    updateUserInfoByUserType
}