import { Controller, Get, Query } from '@nestjs/common';
import { CinemaService } from './cinema.service';
import { ApiQuery, ApiTags } from '@nestjs/swagger';


@ApiTags("QuanLyRap")
@Controller('cinema')
export class CinemaController {
  constructor(private readonly cinemaService: CinemaService) {}

  //lấy thông tin hệ thống rạp
  @ApiQuery({name:"ma_he_thong_rap",required:false})
  @Get("get-cinema-branch")
  getCinemaBranch(
    @Query("ma_he_thong_rap") ma_he_thong_rap :string,
  ){
    return this.cinemaService.getCinemaBranch(ma_he_thong_rap);
  }

  //lấy thông tin cụm rap theo hệ thống rạp
  @Get("get-cinema-group")
  getCinemaGroupByBranch(
    @Query("ma_he_thong_rap") ma_he_thong_rap :string,
  ){
    return this.cinemaService.getCinemaGroupByBranch(ma_he_thong_rap);
  }

  //lấy thông tin lịch chiếu phim theo hệ thống rạp
  @Get("get-movie-show-time")
  getMovieShowTimeByBranch(
    @Query("ma_he_thong_rap") ma_he_thong_rap :string,
  ){
    return this.cinemaService.getMovieShowTimeByBranch(ma_he_thong_rap);
  }

   //lấy thông tin lịch chiếu phim theo hệ thống rạp
   @Get("get-movie-show-time-by-id")
   getMovieShowTimeByID(
     @Query("ma_phim") ma_phim :string,
   ){
     return this.cinemaService.getMovieShowTimeByID(Number(ma_phim));
   }
}
