import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { ResponeService } from 'src/message/res.message';

@Injectable()
export class CinemaService {

    prisma = new PrismaClient();
    
    constructor(private resService : ResponeService){}

    
    async getCinemaBranch(ma_he_thong_rap:string) {
        let data = await this.prisma.he_thong_rap.findMany(
            {
                where:{
                    ma_he_thong_rap:{
                        contains:ma_he_thong_rap
                    }
                },
            }
        );
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }    
    }

    async getCinemaGroupByBranch(ma_he_thong_rap:string) {
        let data = await this.prisma.cum_rap.findMany(
            {
                where:{
                    ma_he_thong_rap
                },
                include:{
                    rap_phim:{
                        select:{
                            ma_rap:true,
                            ten_rap:true
                        }
                    }
                }
            }
        );
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }    
    }

    async getMovieShowTimeByBranch(ma_he_thong_rap:string) {
        let data = await this.prisma.cum_rap.findMany(
            {
                where:{
                    ma_he_thong_rap
                },
                include:{
                    rap_phim:{
                        include:{
                            lich_chieu :{
                                include :{
                                    phim : true
                                }
                            }
                        }
                    }
                }
            }
        );
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }    
    }

    async getMovieShowTimeByID(ma_phim:number) {
        let data = await this.prisma.lich_chieu.findMany(
            {
                where:{
                    ma_phim
                },
                include:{
                    rap_phim:{
                        include:{
                            cum_rap :{
                                include :{
                                    he_thong_rap : true
                                }
                            }
                        }
                    }
                }
            }
        );
        if(data.length>0){
            return this.resService.successCode(data,"Get Success");
        }else{
            return this.resService.failCode([],"Data not found");
        }    
    }
}
