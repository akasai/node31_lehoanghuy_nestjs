import { Module } from '@nestjs/common';
import { CinemaService } from './cinema.service';
import { CinemaController } from './cinema.controller';
import { ResponeService } from 'src/message/res.message';

@Module({
  controllers: [CinemaController],
  providers: [CinemaService,ResponeService]
})
export class CinemaModule {}
