import { Injectable } from '@nestjs/common';

@Injectable()
export class ResponeService {

    successCode =(data,message)=>{
        return {
            statusCode:200,
            message,
            content: data,
            datetime : new Date()
        }
    }
    
    failCode =(data,message)=>{
        return {
            statusCode:400,
            message,
            content: data,
            datetime : new Date()
        }
    }
}