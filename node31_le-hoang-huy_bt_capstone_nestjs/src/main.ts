import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from "express";
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(express.static("."))
  app.useGlobalPipes(new ValidationPipe())
  const config = new DocumentBuilder().setTitle("NestJS Movie").addBearerAuth().build();
  const document = SwaggerModule.createDocument(app,config);

  //localhost:8080/swagger
  SwaggerModule.setup("swagger",app,document);

  await app.listen(8080);
}
bootstrap();
