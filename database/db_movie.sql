-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_movie
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banner` (
  `ma_banner` int NOT NULL AUTO_INCREMENT,
  `ma_phim` int DEFAULT NULL,
  `hinh_anh` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ma_banner`),
  KEY `ma_phim` (`ma_phim`),
  CONSTRAINT `banner_ibfk_1` FOREIGN KEY (`ma_phim`) REFERENCES `phim` (`ma_phim`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,1,'https://cdn.galaxycine.vn/media/2023/6/30/mission-500x750_1688095234240.jpg'),(2,2,'https://cdn.galaxycine.vn/media/2023/7/8/conan-movie-26-1_1688781407463.jpg'),(3,4,'https://cdn.galaxycine.vn/media/2023/4/4/mv5bmjy4o…keyxkfqcgdeqxvyodk4otc3mty--v1-_1680580348614.jpg');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cum_rap`
--

DROP TABLE IF EXISTS `cum_rap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cum_rap` (
  `ma_cum_rap` varchar(150) NOT NULL,
  `ten_cum_rap` varchar(150) DEFAULT NULL,
  `dia_chi` varchar(150) DEFAULT NULL,
  `ma_he_thong_rap` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ma_cum_rap`),
  KEY `ma_he_thong_rap` (`ma_he_thong_rap`),
  CONSTRAINT `cum_rap_ibfk_1` FOREIGN KEY (`ma_he_thong_rap`) REFERENCES `he_thong_rap` (`ma_he_thong_rap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cum_rap`
--

LOCK TABLES `cum_rap` WRITE;
/*!40000 ALTER TABLE `cum_rap` DISABLE KEYS */;
INSERT INTO `cum_rap` VALUES ('cgv-aeon-binh-tan','CGV - Aeon Bình Tân','Tầng 3, TTTM Aeon Mall Bình Tân, Số 1 đường số 17A, khu phố 11, Bình Trị Đông B, Bình Tân','CGV'),('cgv-aeon-tan-phu','CGV - Aeon Tân Phú','30 Bờ Bao Tân Thắng, Sơn Kỳ, Tân Phú','CGV'),('cgv-crescent-mall','CGV - Crescent Mall','Lầu 5, Crescent Mall, Đại lộ Nguyễn Văn Linh, Phú Mỹ Hưng, Q. 7','CGV'),('glx-nguyen-van-qua','GLX -  Nguyễn Văn Quá','119B Nguyễn Văn Quá, Đông Hưng Thuận, Q.12, TPHCM','Galaxy'),('glx-quang-trung','GLX - Quang Trung','L3-Co.opmart Foodcosa, 304A Quang Trung, Gò Vấp','Galaxy');
/*!40000 ALTER TABLE `cum_rap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dat_ve`
--

DROP TABLE IF EXISTS `dat_ve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dat_ve` (
  `ma_dat_ve` int NOT NULL AUTO_INCREMENT,
  `ma_tai_khoan` int DEFAULT NULL,
  `ma_lich_chieu` int DEFAULT NULL,
  `ma_ghe` int DEFAULT NULL,
  PRIMARY KEY (`ma_dat_ve`),
  KEY `ma_tai_khoan` (`ma_tai_khoan`),
  KEY `ma_lich_chieu` (`ma_lich_chieu`),
  CONSTRAINT `dat_ve_ibfk_1` FOREIGN KEY (`ma_tai_khoan`) REFERENCES `nguoi_dung` (`ma_tai_khoan`),
  CONSTRAINT `dat_ve_ibfk_2` FOREIGN KEY (`ma_lich_chieu`) REFERENCES `lich_chieu` (`ma_lich_chieu`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dat_ve`
--

LOCK TABLES `dat_ve` WRITE;
/*!40000 ALTER TABLE `dat_ve` DISABLE KEYS */;
INSERT INTO `dat_ve` VALUES (4,2,1,1111),(5,2,1,1113),(6,14,1,1111),(7,14,1,1111),(8,1,1,1111);
/*!40000 ALTER TABLE `dat_ve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ghe`
--

DROP TABLE IF EXISTS `ghe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ghe` (
  `ma_ghe` int NOT NULL,
  `ten_ghe` varchar(150) DEFAULT NULL,
  `loai_ghe` varchar(150) DEFAULT NULL,
  `ma_rap` int DEFAULT NULL,
  `is_available` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ma_ghe`),
  KEY `ma_rap` (`ma_rap`),
  CONSTRAINT `ghe_ibfk_1` FOREIGN KEY (`ma_rap`) REFERENCES `rap_phim` (`ma_rap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ghe`
--

LOCK TABLES `ghe` WRITE;
/*!40000 ALTER TABLE `ghe` DISABLE KEYS */;
INSERT INTO `ghe` VALUES (1111,'A1','Thuong',111,0),(1112,'A2','Thuong',111,1),(1113,'B1','Vip',111,1),(1121,'A1','Thuong',112,1),(1122,'A2','Thuong',112,1),(1123,'B1','Vip',112,1),(1131,'A1','Thuong',113,1),(1132,'A2','Thuong',113,1),(1133,'B1','Vip',113,1),(1211,'A1','Thuong',121,1),(1212,'A2','Thuong',121,1),(1213,'B1','Vip',121,1),(1221,'A1','Thuong',122,1),(1222,'A2','Thuong',122,1),(1223,'B1','Vip',122,1),(1232,'A2','Thuong',123,1),(1311,'A1','Thuong',131,1),(1321,'A1','Thuong',132,1),(4413,'B1','Vip',441,1),(4513,'B1','Vip',451,1);
/*!40000 ALTER TABLE `ghe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `he_thong_rap`
--

DROP TABLE IF EXISTS `he_thong_rap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `he_thong_rap` (
  `ma_he_thong_rap` varchar(150) NOT NULL,
  `ten_he_thong_rap` varchar(150) DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ma_he_thong_rap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `he_thong_rap`
--

LOCK TABLES `he_thong_rap` WRITE;
/*!40000 ALTER TABLE `he_thong_rap` DISABLE KEYS */;
INSERT INTO `he_thong_rap` VALUES ('CGV','CGV','https://www.cgv.vn/skin/frontend/cgv/default/images/cgvlogo.png'),('Cinemax','Cinemax','https://theme.hstatic.net/1000296517/1000449871/14/logo.png?v=6999'),('CineStar','Cinestar','https://cinestar.com.vn/pictures/moi/9Logo/white-2018.png'),('Galaxy','Galaxy','https://www.galaxycine.vn/website/images/galaxy-logo.png'),('LotteCinima','LotteCinima','https://media.lottecinemavn.com/Media/WebAdmin/ccc95ee5b9274a12ba3e51317250dcbe.png');
/*!40000 ALTER TABLE `he_thong_rap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lich_chieu`
--

DROP TABLE IF EXISTS `lich_chieu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lich_chieu` (
  `ma_lich_chieu` int NOT NULL AUTO_INCREMENT,
  `ma_rap` int DEFAULT NULL,
  `ma_phim` int DEFAULT NULL,
  `ngay_gio_chieu` datetime DEFAULT NULL,
  `gia_ve` int DEFAULT NULL,
  PRIMARY KEY (`ma_lich_chieu`),
  KEY `ma_rap` (`ma_rap`),
  KEY `ma_phim` (`ma_phim`),
  CONSTRAINT `lich_chieu_ibfk_1` FOREIGN KEY (`ma_rap`) REFERENCES `rap_phim` (`ma_rap`),
  CONSTRAINT `lich_chieu_ibfk_2` FOREIGN KEY (`ma_phim`) REFERENCES `phim` (`ma_phim`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lich_chieu`
--

LOCK TABLES `lich_chieu` WRITE;
/*!40000 ALTER TABLE `lich_chieu` DISABLE KEYS */;
INSERT INTO `lich_chieu` VALUES (1,111,1,'2023-07-08 08:00:00',100000),(2,111,1,'2023-07-08 15:00:00',100000),(3,112,2,'2023-07-21 08:00:00',70000),(4,113,3,'2023-07-14 08:00:00',100000),(5,121,2,'2023-07-21 08:00:00',120000),(6,122,4,'2023-08-18 08:00:00',150000),(7,123,1,'2023-07-08 08:00:00',100000),(8,131,2,'2023-07-21 08:00:00',80000),(9,132,3,'2023-07-14 18:00:00',80000),(10,441,4,'2023-08-18 21:00:00',50000),(11,451,1,'2023-07-08 22:00:00',75000),(12,113,5,'2023-11-10 01:00:00',15000),(13,451,5,'2023-08-17 00:00:00',50000),(14,132,4,'2023-08-07 17:00:00',100000);
/*!40000 ALTER TABLE `lich_chieu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nguoi_dung`
--

DROP TABLE IF EXISTS `nguoi_dung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nguoi_dung` (
  `ma_tai_khoan` int NOT NULL AUTO_INCREMENT,
  `tai_khoan` varchar(100) NOT NULL,
  `ho_ten` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `so_dt` varchar(50) DEFAULT NULL,
  `mat_khau` varchar(250) NOT NULL,
  `loai_nguoi_dung` int DEFAULT NULL,
  `vai_tro` varchar(50) DEFAULT NULL,
  `ma_nhom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ma_tai_khoan`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nguoi_dung`
--

LOCK TABLES `nguoi_dung` WRITE;
/*!40000 ALTER TABLE `nguoi_dung` DISABLE KEYS */;
INSERT INTO `nguoi_dung` VALUES (1,'huytest01','HUY LE','huytestest@gmail.com','12345','$2b$10$iDD39y5mA911frsb5Rlase.4M5D7H1YLJwXON9s7BZx4ySwjWL8D2',0,'Admin','Galaxy'),(2,'drowranger','Traxex','drowranger@gmail.com','18001001','$2b$10$iDD39y5mA911frsb5Rlase.4M5D7H1YLJwXON9s7BZx4ySwjWL8D2',1,'User','Galaxy'),(3,'windranger','windranger','windranger@gmail.com','18001002','$2b$10$obvzKUpByxQbu/GL4xTJkuaKd9K9IsqjJ/E1s2ouRthDg4j4b7r1m',1,'User','Galaxy'),(4,'phantom','phantom','phantom@gmail.com','0912345678','$2b$10$avXf.sG9cIpr7vP3/mf8OeRDy2G3.hArOQKPc1UyDIX5TKE2e6ch2',1,'User','Galaxy'),(5,'medusa','Gorgon','medusa@gmail.com','974586321','$2b$10$3uwRRLedIlyAJM4wuLNIe.1RSN6hnNFtHeKy/IKIz7D4MkzKt7ujW',1,'User','Galaxy'),(6,'huytest02','Le Hoang Huy','huytest@gmail.com','0987654321','$2b$10$IzbOvd7RANXgkcQtkd9Kd.tIkCJ8Sa4tz1LEVI8QE8UNZvBwborxa',0,'Admin','CGV'),(7,'tsubasa','AAA','HAHAHA@gmail.com','111','$2b$10$wa8sBbAs2NeddWO9CzRXa.kZ1f1d7mlp8lm5zmbdrewKTLTlfEGEy',0,'Admin','BHD'),(8,'kojiro','Kojiro Hyuga','kojiro@gmail.com','18001002','$2b$10$obvzKUpByxQbu/GL4xTJkuaKd9K9IsqjJ/E1s2ouRthDg4j4b7r1m',1,'User','CGV'),(9,'genzo','Wakabayashi Genzo','genzo@gmail.com','18001003','$2b$10$avXf.sG9cIpr7vP3/mf8OeRDy2G3.hArOQKPc1UyDIX5TKE2e6ch2',1,'User','CGV'),(10,'misaki','Misaki Taro','misaki@gmail.com','18001004','$2b$10$e41tzbSlkLvSsBTEZ9.X5.P3qUjIMm7/1fLFlXhUt0BTBAdG0JJS2',1,'User','CGV'),(11,'vildred','vildred','arbitervildred@gmail.com','8799413','$2b$10$oVHFnbD8KoHhgespywlHaeBRfH2tSGbnyTgIcD1ldAk2q8JfyWZ8O',1,'User','EP7'),(12,'ravi','Ravi','ravi@gmail.com','18799413','$2b$10$/bDjTUgXhO4eDpZ1GC08x.D/5oD5ljsXiMMi/qhbjLr7ARBVpOM7C',1,'User','EP7'),(14,'huytest03','huy','test@gmail.com','9875412','$2b$10$k/K/Moe5jYnOfXbheaG.y.XtyacHkENz0d4QpuqWR.wR98yvJfK2q',1,'User','GLX'),(15,'huytest05','string','string@gmail.com','1234','$2b$10$GSLi8yUSEaRfY.WR9xZXVOrrP189RKJANWLSTnW5iNbpVZ3YdIi1a',1,'User','Galaxy'),(16,'huytest06','huy','string@gmail.com','112314','$2b$10$Q28u.E8ixP0TfF0hXoZz7.QaCQINT/H6fNq777blPacfRffrp8ojW',1,'User','Galaxy');
/*!40000 ALTER TABLE `nguoi_dung` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phim`
--

DROP TABLE IF EXISTS `phim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phim` (
  `ma_phim` int NOT NULL AUTO_INCREMENT,
  `ten_phim` varchar(150) DEFAULT NULL,
  `trailer` varchar(150) DEFAULT NULL,
  `hinh_anh` varchar(150) DEFAULT NULL,
  `mo_ta` varchar(150) DEFAULT NULL,
  `ngay_khoi_chieu` datetime DEFAULT NULL,
  `danh_gia` int DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `dang_chieu` tinyint(1) DEFAULT NULL,
  `sap_chieu` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ma_phim`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phim`
--

LOCK TABLES `phim` WRITE;
/*!40000 ALTER TABLE `phim` DISABLE KEYS */;
INSERT INTO `phim` VALUES (1,'NHIỆM VỤ: BẤT KHẢ THI NGHIỆP BÁO PHẦN MỘT','https://www.youtube.com/watch?v=TVEIIUXUC6E','https://cdn.galaxycine.vn/media/2023/6/30/mission-500x750_1688095234240.jpg','Câu chuyện lần này diễn ra khi Ethan...','2023-07-08 08:00:00',8,1,1,0),(2,'DETECTIVE CONAN: BLACK IRON SUBMARINE','https://www.youtube.com/watch?v=0rNpSmVmN2U','	https://cdn.galaxycine.vn/media/2023/7/8/conan-movie-26-1_1688781407463.jpg','Lấy bối cảnh tại Pacific Buoy...','2023-07-21 08:00:00',9,1,0,1),(3,'INSIDIOUS: THE RED DOOR','https://www.youtube.com/watch?v=0zIYmS0wosA','	https://cdn.galaxycine.vn/media/2023/7/14/500x750-insi_1689307032412.jpg','Insidious 5: The Red Door lấy bối cảnh sau mười năm ...','2023-07-14 08:00:00',8,0,1,0),(4,'BLUE BEETLE','https://www.youtube.com/watch?v=4wxyy8Rcz4k','	https://cdn.galaxycine.vn/media/2023/4/4/mv5bmjy4o…keyxkfqcgdeqxvyodk4otc3mty--v1-_1680580348614.jpg','Xoay quanh thế hệ thứ 3 của siêu anh hùng này...','2023-08-18 08:00:00',10,1,0,1),(5,'THE MARVELS','https://www.youtube.com/watch?v=iuk77TjvfmE','https://cdn.galaxycine.vn/media/2023/6/13/fplyeszx0aek-ev_1686642231637.jpg','The Marvels','2023-11-10 08:00:00',10,1,0,1);
/*!40000 ALTER TABLE `phim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rap_phim`
--

DROP TABLE IF EXISTS `rap_phim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rap_phim` (
  `ma_rap` int NOT NULL,
  `ten_rap` varchar(150) DEFAULT NULL,
  `ma_cum_rap` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ma_rap`),
  KEY `ma_cum_rap` (`ma_cum_rap`),
  CONSTRAINT `rap_phim_ibfk_1` FOREIGN KEY (`ma_cum_rap`) REFERENCES `cum_rap` (`ma_cum_rap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rap_phim`
--

LOCK TABLES `rap_phim` WRITE;
/*!40000 ALTER TABLE `rap_phim` DISABLE KEYS */;
INSERT INTO `rap_phim` VALUES (111,'Rạp 1','cgv-aeon-binh-tan'),(112,'Rạp 2','cgv-aeon-binh-tan'),(113,'Rạp 3','cgv-aeon-binh-tan'),(121,'Rạp 1','cgv-aeon-tan-phu'),(122,'Rạp 2','cgv-aeon-tan-phu'),(123,'Rạp 3','cgv-aeon-tan-phu'),(131,'Rạp 1','cgv-crescent-mall'),(132,'Rạp 2','cgv-crescent-mall'),(441,'Rạp 1','glx-nguyen-van-qua'),(451,'Rạp 1','glx-quang-trung');
/*!40000 ALTER TABLE `rap_phim` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-15 20:43:33
